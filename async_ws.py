#!/usr/bin/env python
# coding=utf-8
import asyncio
import websockets

NEXGOURL = 'wss://www.nexbang.cn/smarthome/websocket'

async def cows(uri,num):
    async with websockets.connect(uri) as websocket:
        #await websocket.send("Hello world!")
        await asyncio.sleep(60-(0.01*num))
        return num 

async def main(num_phases):  
    print('starting main')  
    wslist = [
        cows(NEXGOURL,i) 
        for i in range(num_phases)
    ]

    print('waiting for wslist to complete')
    #results = []  
    for next_to_complete in asyncio.as_completed(wslist):
        answer = await next_to_complete 
        print('running:{!r}'.format(answer))
        #results.append(answer)

    #print('results: {!r}'.format(results))  
    return 0

event_loop = asyncio.get_event_loop()  
try:  
    event_loop.run_until_complete(main(3000))  
finally:  
    event_loop.close()
