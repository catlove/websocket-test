#!/usr/bin/env python
# coding=utf-8

from concurrent.futures import ThreadPoolExecutor as Pool
from concurrent.futures import wait
import nexgocli

Thread_num=1023

WEB_URL="wss://www.nexbang.cn/smarthome/websocket"

def task(url):
    return nexgocli.web_test_thread(url)

with Pool(max_workers=Thread_num) as executor:
    future_tasks = [executor.submit(task, WEB_URL) for i in range(1,Thread_num)]

    for f in future_tasks:
        if f.running():
            Thread_num = Thread_num - 1
            print('%d is running' % Thread_num)

    results = wait(future_tasks)
    
    done = results[0]
    for x in done:
        print(x)
