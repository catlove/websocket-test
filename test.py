#!/usr/bin/env python
# coding=utf-8

import asyncio
import websockets

async def hello(uri):
    async with websockets.connect(uri) as websocket:
        await websocket.send("Hello world!")
        greeting = await websocket.recv()
        print("< {}".format(greeting))

asyncio.get_event_loop().run_until_complete(
    hello('wss://www.nexbang.cn/smarthome/websocket')
    )
